import environ

env = environ.Env(
    DEBUG=(bool, False)
)
root = environ.Path(__file__) - 2
environ.Env.read_env(root('.env'))
