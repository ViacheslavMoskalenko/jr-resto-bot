from django.urls import path

import telegrambot.restaurant.views as views

urlpatterns = [
   path('', views.add_restaurant)
]
