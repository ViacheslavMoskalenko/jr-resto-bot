from collections import namedtuple

import telebot
from django.core.management import BaseCommand

from telegrambot.env import env
from telegrambot.restaurant.utils import get_help_info, set_user_step, get_user_step, reset_user_provided_data, \
    set_user_provided_title, set_user_provided_address, set_user_provided_photo, add_restaurant, \
    get_user_draft, fetch_latest_added_restaurants, remove_user_added_restaurants

KNOWN_COMMANDS = namedtuple('UserCommands', 'ADD LIST RESET START')('/add', '/list', '/reset', '/start')
bot = telebot.TeleBot(env('TELEGRAM_BOT_KEY'))
STEPS = namedtuple('UserDataFillingProgress', 'TITLE ADDRESS PHOTO NOT_STARTED')(1, 2, 3, -1)


def is_step(step_code):
    return lambda message: get_user_step(message.from_user.id) == step_code


def is_unknown_command(message):
    text = message.json.get('text')
    return isinstance(text, str) and text.startswith('/') and text not in KNOWN_COMMANDS


@bot.message_handler(content_types=['text'], func=is_unknown_command)
def handle_unknown_command(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    bot.send_message(user_id, 'Sorry. I don\' t understand this command. Try typing /start')


@bot.message_handler(func=lambda message: message.from_user.is_bot)
def handle_other_bot_message(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    bot.send_message(user_id, 'Sorry. I don\'t talk to other bots.')


@bot.message_handler(func=lambda message: message.chat.type != 'private')
def handle_group_chat_message(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    bot.send_message(user_id, 'Sorry. I only support messages from private chats.')


@bot.message_handler(commands=['reset'])
def handle_restaurant_reset(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    remove_user_added_restaurants(user_id)
    bot.send_message(user_id, 'All your previously added data has been removed')


@bot.message_handler(commands=['start'])
def handle_start_request(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    bot.send_message(user_id, get_help_info())


@bot.message_handler(commands=['add'])
def handle_add_request(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    set_user_step(user_id, 1)
    reset_user_provided_data(user_id)
    bot.send_message(user_id, 'Ok. What is the name of a restaurant?')


@bot.message_handler(commands=['list'])
def handle_restaurant_list(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    restaurants = fetch_latest_added_restaurants(user_id)
    if not restaurants:
        bot.send_message(user_id, 'It seems you don\'t have any added data. Try adding one by typing /add')
    else:
        bot.send_message(user_id, 'Here are the few last added restaurants:'.format(len(restaurants)))
        for restaurant in restaurants:
            caption = '"{}" at {}'.format(restaurant.title, restaurant.address)
            bot.send_photo(user_id, restaurant.photo, caption)


@bot.message_handler(func=is_step(STEPS.TITLE), content_types=['text'])
def handle_add_restaurant_title(message):
    restaurant_title = message.json.get('text')
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    set_user_provided_title(user_id, restaurant_title)
    set_user_step(user_id, STEPS.ADDRESS)
    bot.send_message(message.from_user.id, 'Got it. What is the restaurant address?')


@bot.message_handler(func=is_step(STEPS.ADDRESS), content_types=['text'])
def handle_add_restaurant_address(message):
    restaurant_address = message.json.get('text')
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    set_user_provided_address(user_id, restaurant_address)
    set_user_step(user_id, STEPS.PHOTO)
    bot.send_message(message.from_user.id, 'Yep. Please, attach the restaurant\'s photo (not as "a file").')


@bot.message_handler(func=is_step(STEPS.PHOTO), content_types=['photo'])
def handle_add_restaurant_photo(message):
    user_id = message.from_user.id
    bot.send_chat_action(user_id, 'typing')
    photo_file_id = ''
    photo_max_size = 0
    for photo in message.photo:
        if photo.file_size > photo_max_size:
            photo_max_size = photo.file_size
            photo_file_id = photo.file_id
    set_user_provided_photo(user_id, photo_file_id)
    user_draft = get_user_draft(user_id)
    add_restaurant(
        title=user_draft.get('title'),
        address=user_draft.get('address'),
        photo=user_draft.get('photo'),
        author=user_draft.get('user_id')
    )
    bot.send_message(user_id, 'Done! Feel free to add another one by typing /add')
    set_user_step(user_id, STEPS.NOT_STARTED)


def start_telegram_bot(telegram_bot):
    print('Initializing the Telegram bot')
    telegram_bot.polling(none_stop=False)


def stop_telegram_bot(telegram_bot):
    print('Stop the Telegram bot')
    telegram_bot.stop_bot()


class Command(BaseCommand):
    help = 'Launches the jr-resto-bot that can help user with adding a restaurant in three steps  `'

    def handle(self, *args, **kwargs):
        start_telegram_bot(bot)
