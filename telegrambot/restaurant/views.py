# Create your views here.
import json
from json import JSONDecodeError

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

import telegrambot.restaurant.utils as utils


@require_http_methods(['POST'])
@csrf_exempt
def add_restaurant(request):
    answer = JsonResponse({'result': 'success'}, status=201)
    try:
        data = json.loads(request.body.decode())
        title = data.get('title', '')
        address = data.get('address', '')
        photo = data.get('photo', '')
        author = '12345678'
        utils.add_restaurant(title, address, photo, author)
    except JSONDecodeError:
        answer = JsonResponse({'result': 'Unable to decode payload as json'}, status=400)
    return answer
