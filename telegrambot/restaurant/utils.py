from django.db import transaction
from django.forms import model_to_dict

from telegrambot.restaurant.models import Restaurant, UserDataFillingProgress, UserDraft


def add_restaurant(title, address, photo, author):
    Restaurant.objects.create(title=title, address=address, photo=photo, author=str(author))


def set_user_provided_title(user_id, title):
    user_id = str(user_id)
    UserDraft.objects.filter(user_id=user_id).update_or_create(defaults={
        'user_id': str(user_id),
        'title': title
    })


def set_user_provided_address(user_id, address):
    UserDraft.objects.filter(user_id=user_id).update_or_create(defaults={
        'user_id': str(user_id),
        'address': address
    })


def set_user_provided_photo(user_id, photo):
    UserDraft.objects.filter(user_id=user_id).update_or_create(defaults={
        'user_id': str(user_id),
        'photo': photo
    })


def get_user_step(user_id):
    user_progress = UserDataFillingProgress.objects.filter(user_id=str(user_id))
    return user_progress[0].step if user_progress else -1


def get_user_draft(user_id):
    matches = UserDraft.objects.filter(user_id=str(user_id))
    user_draft = matches[0] if matches else None
    default_draft = {
        'user_id': '',
        'title': '',
        'address': '',
        'photo': ''
    }
    return model_to_dict(user_draft, fields=default_draft.keys()) if user_draft else default_draft


def set_user_step(user_id, step):
    UserDataFillingProgress.objects.filter(user_id=user_id).update_or_create(defaults={
        'user_id': str(user_id),
        'step': step
    })


def reset_user_provided_data(user_id):
    set_user_provided_title(user_id, '')
    set_user_provided_address(user_id, '')
    set_user_provided_photo(user_id, '')


def fetch_latest_added_restaurants(user_id):
    return Restaurant.objects.order_by('-created_on').filter(author=str(user_id))[:10]


def remove_user_added_restaurants(user_id):
    Restaurant.objects.filter(author=str(user_id)).delete()


def get_help_info():
    text = """
I am a bot that can help you to add a restaurant in three steps:
1) title
2) address
3) photo
I can react to these commands:
* /add - to add a new place
* /list - to display the last 10 added items
* /reset - to delete all the previously added places
    """
    return text


@transaction.atomic
def wipe_all():
    """
    Wipes all the data that has been previously stored
    Warning: you're going to need your manager approval
    to execute this method
    """
    Restaurant.objects.all().delete()
    UserDraft.objects.all().delete()
    UserDataFillingProgress.objects.all().delete()
