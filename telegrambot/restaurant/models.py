from django.db import models
from django.utils.html import strip_tags


# Create your models here.
class Restaurant(models.Model):
    title = models.CharField(max_length=128)
    address = models.CharField(max_length=255)
    photo = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=64)

    def save(self, *args, **kwargs):
        self.title = strip_tags(self.title)
        self.address = strip_tags(self.address)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['created_on']


class UserDataFillingProgress(models.Model):
    user_id = models.CharField(max_length=64)
    last_modified = models.DateTimeField(auto_now_add=True)
    step = models.IntegerField()

    class Meta:
        ordering = ['last_modified']


class UserDraft(models.Model):
    user_id = models.CharField(max_length=64)
    last_modified = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=128, default='')
    address = models.CharField(max_length=255, default='')
    photo = models.TextField(default='')

    class Meta:
        ordering = ['last_modified']
