from django.urls import path, include

urlpatterns = [
    path('restaurant/', include('telegrambot.restaurant.urls'))
]
